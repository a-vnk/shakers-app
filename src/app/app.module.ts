import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from 'src/components/common/header/header.component';
import { FilterAlcoholComponent } from 'src/components/filter/filter-alcohol/filter-alcohol.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FilterAlcoholComponent,
    FilterAlcoholComponent,
  ],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
