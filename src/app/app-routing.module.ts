import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from 'src/components/common/homepage/homepage.component';
import { FilterAlcoholComponent } from 'src/components/filter/filter-alcohol/filter-alcohol.component';
import { FilterFlavorComponent } from 'src/components/filter/filter-flavor/filter-flavor.component';
import { ResultComponent } from 'src/components/result/result.component';

const routes: Routes = [
  {
    path: '',
    component: HomepageComponent,
  },
  {
    path: 'filter/alcohol',
    component: FilterAlcoholComponent,
  },
  {
    path: 'filter/flavor',
    component: FilterFlavorComponent,
  },
  {
    path: 'result',
    component: ResultComponent,
  },
];

@NgModule({
  declarations: [HomepageComponent],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
