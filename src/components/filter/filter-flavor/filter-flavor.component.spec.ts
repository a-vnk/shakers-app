import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterFlavorComponent } from './filter-flavor.component';

describe('FilterFlavorComponent', () => {
  let component: FilterFlavorComponent;
  let fixture: ComponentFixture<FilterFlavorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilterFlavorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FilterFlavorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
