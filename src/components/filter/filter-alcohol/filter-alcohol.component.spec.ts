import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterAlcoholComponent } from './filter-alcohol.component';

describe('FilterAlcoholComponent', () => {
  let component: FilterAlcoholComponent;
  let fixture: ComponentFixture<FilterAlcoholComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilterAlcoholComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FilterAlcoholComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
